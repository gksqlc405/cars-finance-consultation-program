# 자동차 금융 상담 프로그램
***
자동차 금융 상담내역들을 관리하는 프로그램<br />
[참고사이트](https://auto.daum.net/newcar)
***


#### VERSION
````
0.0.1
````

#### LANGUAGE
```
JAVA 14
SpringBoot 2.7.3
```
#### 기능
```
* 상담내역 관리
   - 상담내역 정보 가저오기 (Get)
   - 상담내역 정보 수정 (Put)
   - 상담내역 날짜 확인 (Get)
   - 상담내역 등록 (Post)
   - 회원 전화번호 수정 (Put)
   
* 차량 정보 관리
   - 제조사 정보 가저오기 (Get)
   - 제조사 정보 등록 (Post)
   - 제조사명 수정 (Put)
   - 차량모델 정보 수저아기 (Put)
   - 연료타입 수정 (Put)
   - 차량 모델 정보 가저오기 (Get)
   - 차량 모델 정보 등록 (Post)  
   - 차량 등급 등록 (Post)
   - 차량 등급 정보 수정 (Put)
   - 트림 정보 등록 (Post)
   - 트림 정보 수정
   - 자동차 디테일 정보 가저오기 (Get)
   - 출시일 수정하기 (Put)
   
```


# app 화면

### 메인화면
![SwaggerMain](./images/car1.png)

### 상담내역 관리
![SwaggerUsageDetail](./images/car2.png)

### 차량 정보 관리
![SwaggerUsageDetail](./images/car3.png)


