package com.chb.carConsultation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarConsultationApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarConsultationApplication.class, args);
	}

}
