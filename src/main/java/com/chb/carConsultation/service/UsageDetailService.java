package com.chb.carConsultation.service;

import com.chb.carConsultation.entity.Trim;
import com.chb.carConsultation.entity.UsageDetail;
import com.chb.carConsultation.exception.CMissingDataException;
import com.chb.carConsultation.model.*;
import com.chb.carConsultation.model.UsageDetail.UsageDetailItem;
import com.chb.carConsultation.model.UsageDetail.UsageDetailPhoneUpdate;
import com.chb.carConsultation.model.UsageDetail.UsageDetailRequest;
import com.chb.carConsultation.repository.UsageDetailRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UsageDetailService {
    private final UsageDetailRepository usageDetailRepository;

    public void setUsageDetail(Trim trim, UsageDetailRequest request) {
        UsageDetail usageDetail = new UsageDetail.UsageDetailBuilder(trim, request).build();
        usageDetailRepository.save(usageDetail);
    }

    public UsageDetailItem getUsageDetail(long id) {
        UsageDetail usageDetails = usageDetailRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new UsageDetailItem.UsageDetailItemBuilder(usageDetails).build();
    }

    public ListResult<UsageDetailItem> getUsageDetails() {
        List<UsageDetailItem> result = new LinkedList<>();

        List<UsageDetail> usageDetails = usageDetailRepository.findAll();
        usageDetails.forEach(usageDetail -> {
            UsageDetailItem addItem = new UsageDetailItem.UsageDetailItemBuilder(usageDetail).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    public void putUsageDetail(long id, UsageDetailRequest request) {
        UsageDetail usageDetail = usageDetailRepository.findById(id).orElseThrow(ConcurrentModificationException::new);
        usageDetail.putUsageDetail(request);
        usageDetailRepository.save(usageDetail);
    }

    public void putPhoneUpdate(long id, UsageDetailPhoneUpdate request) {
        UsageDetail usageDetail = usageDetailRepository.findById(id).orElseThrow(CMissingDataException::new);
        usageDetail.putPhoneUpdate(request);
        usageDetailRepository.save(usageDetail);
    }

    public ListResult<UsageDetailItem> getUsageDetail(LocalDate dateStart, LocalDate dateEnd) {
        LocalDateTime dateStartTime = LocalDateTime.of(
                dateStart.getYear(),
                dateStart.getMonthValue(),
                dateStart.getDayOfMonth(),
                0,
                0,
                0
        );

        LocalDateTime dateEndTime = LocalDateTime.of(
                dateEnd.getYear(),
                dateEnd.getMonthValue(),
                dateEnd.getDayOfMonth(),
                23,
                59,
                59
        );

        List<UsageDetail> usageDetails =
                usageDetailRepository.findAllByDateCreateGreaterThanEqualAndDateCreateLessThanEqualOrderByIdDesc(dateStartTime, dateEndTime);
        List<UsageDetailItem> result = new LinkedList<>();
        usageDetails.forEach(usageDetail -> {
            UsageDetailItem addItem = new UsageDetailItem.UsageDetailItemBuilder(usageDetail).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);

    }

}

