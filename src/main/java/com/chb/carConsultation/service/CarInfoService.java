package com.chb.carConsultation.service;

import com.chb.carConsultation.entity.CarModel;
import com.chb.carConsultation.entity.Grade;
import com.chb.carConsultation.entity.Manufacturer;
import com.chb.carConsultation.entity.Trim;
import com.chb.carConsultation.exception.CMissingDataException;
import com.chb.carConsultation.model.*;
import com.chb.carConsultation.model.carGrade.GradeRequest;
import com.chb.carConsultation.model.carManufacturer.ManufacturerItem;
import com.chb.carConsultation.model.carManufacturer.ManufacturerRequest;
import com.chb.carConsultation.model.carModel.CarListItem;
import com.chb.carConsultation.model.carModel.CarModelItem;
import com.chb.carConsultation.model.carModel.CarModelLaunchUpdateRequest;
import com.chb.carConsultation.model.carModel.CarModelRequest;
import com.chb.carConsultation.model.carTrim.CarInfoDetailResponse;
import com.chb.carConsultation.model.carTrim.FuelTypeUpdateRequest;
import com.chb.carConsultation.model.carTrim.TrimRequest;
import com.chb.carConsultation.repository.CarModelRepository;
import com.chb.carConsultation.repository.GradeRepository;
import com.chb.carConsultation.repository.ManufacturerRepository;
import com.chb.carConsultation.repository.TrimRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class CarInfoService {
    private final ManufacturerRepository manufacturerRepository;
    private final CarModelRepository carModelRepository;
    private final GradeRepository gradeRepository;
    private final TrimRepository trimRepository;

    public Manufacturer getManufacturerId(long id) {
        return manufacturerRepository.findById(id).orElseThrow(CMissingDataException::new);
    }
    public void setManufacturer(ManufacturerRequest request) {
        Manufacturer manufacturer = new Manufacturer.ManufacturerBuilder(request).build();
        manufacturerRepository.save(manufacturer);
    }

    public ManufacturerItem getManufacturer(long id) {
        Manufacturer manufacturer = manufacturerRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new  ManufacturerItem.ManufacturerItemBuilder(manufacturer).build();
    }

    public ListResult<ManufacturerItem> getManufacturers() {
        List<ManufacturerItem> result = new LinkedList<>();

        List<Manufacturer> manufacturers = manufacturerRepository.findAll();
        manufacturers.forEach(manufacturer -> {
            ManufacturerItem addItem = new ManufacturerItem.ManufacturerItemBuilder(manufacturer).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    public void putManufacturer(long id, ManufacturerRequest request) {
        Manufacturer manufacturer = manufacturerRepository.findById(id).orElseThrow(CMissingDataException::new);
        manufacturer.putManufacturer(request);
        manufacturerRepository.save(manufacturer);

    }

    public CarModel getCarModelId(long id) {
        return carModelRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public void setCarModel(Manufacturer manufacturer, CarModelRequest request) {
        CarModel carModel = new CarModel.CarModelBuilder(manufacturer, request).build();
        carModelRepository.save(carModel);
    }

    public ListResult<CarListItem>  getCarListItems() {
        List<CarModel> carModels = carModelRepository.findAll();

        List<CarListItem> result = new LinkedList<>();

        carModels.forEach(carModel -> {
            CarListItem addItem = new CarListItem.CarListItemBuilder(carModel).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    public CarModelItem getCarModels(long id) {
        CarModel carModel = carModelRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new  CarModelItem.CarModelItemBuilder(carModel).build();
    }

    public ListResult<CarModelItem> getCarModels() {
        List<CarModelItem> result = new LinkedList<>();

        List<CarModel> carModels = carModelRepository.findAll();
        carModels.forEach(carModel -> {
            CarModelItem addItem = new CarModelItem.CarModelItemBuilder(carModel).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    public void putLaunchUpdate(long id, CarModelLaunchUpdateRequest launchUpdateRequest) {
        CarModel carModel = carModelRepository.findById(id).orElseThrow(CMissingDataException::new);
        carModel.putLaunchUpdate(launchUpdateRequest);
        carModelRepository.save(carModel);
    }

    public void putCarModel(long id, CarModelRequest request) {
        CarModel carModel = carModelRepository.findById(id).orElseThrow(CMissingDataException::new);
        carModel.putCarModel(request);
        carModelRepository.save(carModel);
    }

    public Grade getGradeId(long id) {
        return gradeRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public void setGrade(CarModel carModel, GradeRequest request) {
        Grade grade = new Grade.GradeBuilder(carModel, request).build();
        gradeRepository.save(grade);
    }
    public void putGrade(long id, GradeRequest request) {
        Grade grade = gradeRepository.findById(id).orElseThrow(CMissingDataException::new);
        grade.putGrade(request);
        gradeRepository.save(grade);
    }

    public Trim getTrimId(long id) {
        return trimRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public void setTrim(Grade grade, TrimRequest request) {
        Trim trim = new Trim.TrimBuilder(grade, request).build();
        trimRepository.save(trim);
    }

    public void putTrim(long id, TrimRequest request) {
        Trim trim = trimRepository.findById(id).orElseThrow(ConcurrentModificationException::new);
        trim.putTrim(request);
        trimRepository.save(trim);
    }

    public void putFuelTpeUpdate(long id, FuelTypeUpdateRequest request) {
        Trim trim = trimRepository.findById(id).orElseThrow(CMissingDataException::new);
        trim.putFuelType(request);
        trimRepository.save(trim);
    }

    public CarInfoDetailResponse getCarInfoDetail(long modelId) {
        CarModel carModel = carModelRepository.findById(modelId).orElseThrow(CMissingDataException::new);

        List<Trim> trims = trimRepository.findAllByGrade_CarModel_Id(carModel.getId());

        ArrayList<Double> prices = new ArrayList<>();
        trims.forEach(trim -> {
            prices.add(trim.getPrice());
        });

        prices.sort(Collections.reverseOrder());

        Double maxPrice = prices.get(0);
        Double minPrice = prices.get(prices.size() - 1);

        List<String> carFuelTypes = new LinkedList<>();
        trims.forEach(trim -> {
            carFuelTypes.add(trim.getFuelType().getName());
        });
        Set<String> tempSet = new HashSet<>(carFuelTypes);
        List<String> carFuelTypesResult = new ArrayList<>(tempSet);

        ArrayList<Integer> displacement = new ArrayList<>();
        trims.forEach(trim -> {
            displacement.add(trim.getDisplacement());
        });

        displacement.sort(Collections.reverseOrder());

        Integer maxDisplacement = displacement.get(0);
        Integer minDisplacement = displacement.get(displacement.size() - 1);

        ArrayList<Double> fuelEfficiency = new ArrayList<>();
        trims.forEach(trim -> {
            fuelEfficiency.add(trim.getCombinedFuel());
        });

        fuelEfficiency.sort(Collections.reverseOrder());

        Double maxFuelEfficiency = fuelEfficiency.get(0);
        Double minFuelEfficiency = fuelEfficiency.get(fuelEfficiency.size() - 1);

        ArrayList<Integer> personnelCount = new ArrayList<>();
        trims.forEach(trim -> {
            personnelCount.add(trim.getPersonnelCount());
        });

        personnelCount.sort(Collections.reverseOrder());

        Integer maxPersonnelCount = personnelCount.get(0);
        Integer minPersonnelCount = personnelCount.get(personnelCount.size() - 1);


        return new CarInfoDetailResponse.CarInfoDetailResponseBuilder(
                carModel,
                minPrice,
                maxPrice,
                carFuelTypesResult,
                minDisplacement,
                maxDisplacement,
                maxFuelEfficiency,
                minFuelEfficiency,
                minPersonnelCount,
                maxPersonnelCount).build();


    }
}
