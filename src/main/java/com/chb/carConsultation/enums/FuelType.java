package com.chb.carConsultation.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FuelType {


    GASOLINE("휘발유"),
    LPG("가스"),
    DIESEL("경유"),
    ELECTRICITY("전기");

    private final String name;
}
