package com.chb.carConsultation.entity;

import com.chb.carConsultation.enums.FuelType;
import com.chb.carConsultation.interfaces.CommonModelBuilder;
import com.chb.carConsultation.model.carTrim.FuelTypeUpdateRequest;
import com.chb.carConsultation.model.carTrim.TrimRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Trim {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gradeId", nullable = false)
    private Grade grade;

    @Column(nullable = false)
    private String trimName;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private FuelType fuelType;

    @Column(nullable = false, length = 20)
    private String engineType;

    @Column(nullable = false)
    private Integer displacement;

    @Column(nullable = false, length = 20)
    private String driveMethod;

    @Column(nullable = false)
    private Integer highOutput;

    @Column(nullable = false)
    private Double maximumTorque;

    @Column(nullable = false)
    private Double combinedFuel;

    @Column(nullable = false)
    private Double cityFuel;

    @Column(nullable = false)
    private Double highwayFuel;

    @Column(nullable = false)
    private Integer fuelRating;

    @Column(nullable = false)
    private Double co2Emissions;

    @Column(nullable = false)
    private Integer lowPollutionGrade;

    @Column(nullable = false)
    private Double toleranceWeight;

    @Column(nullable = false)
    private Integer maximumSpeed;

    @Column(nullable = false)
    private Double zeroBack;

    @Column(nullable = false)
    private Integer selfDrivingLevel;

    @Column(nullable = false, length = 20)
    private String frontTireSpecification;

    @Column(nullable = false, length = 20)
    private String rearTireSpecification;

    @Column(nullable = false)
    private Integer personnelCount;

    public void putFuelType(FuelTypeUpdateRequest request) {
        this.fuelType = request.getFuelType();

    }

    public void putTrim(TrimRequest request) {
        this.trimName = request.getTrimName();
        this.price = request.getPrice();
        this.fuelType = request.getFuelType();
        this.engineType = request.getEngineType();
        this.displacement = request.getDisplacement();
        this.driveMethod = request.getDriveMethod();
        this.highOutput = request.getHighOutput();
        this.maximumTorque = request.getMaximumTorque();
        this.combinedFuel = request.getCombinedFuel();
        this.cityFuel = request.getCityFuel();
        this.highwayFuel = request.getHighwayFuel();
        this.fuelRating = request.getFuelRating();
        this.co2Emissions = request.getCo2Emissions();
        this.lowPollutionGrade = request.getLowPollutionGrade();
        this.toleranceWeight = request.getToleranceWeight();
        this.maximumSpeed = request.getMaximumSpeed();
        this.zeroBack = request.getZeroBack();
        this.selfDrivingLevel = request.getSelfDrivingLevel();
        this.frontTireSpecification = request.getFrontTireSpecification();
        this.rearTireSpecification = request.getRearTireSpecification();
    }

    private Trim(TrimBuilder builder) {
        this.grade = builder.grade;
        this.trimName = builder.trimName;
        this.price = builder.price;
        this.fuelType = builder.fuelType;
        this.engineType = builder.engineType;
        this.displacement = builder.displacement;
        this.driveMethod = builder.driveMethod;
        this.highOutput = builder.highOutput;
        this.maximumTorque = builder.maximumTorque;
        this.combinedFuel = builder.combinedFuel;
        this.cityFuel = builder.cityFuel;
        this.highwayFuel = builder.highwayFuel;
        this.fuelRating = builder.fuelRating;
        this.co2Emissions = builder.co2Emissions;
        this.lowPollutionGrade = builder.lowPollutionGrade;
        this.toleranceWeight = builder.toleranceWeight;
        this.maximumSpeed = builder.maximumSpeed;
        this.zeroBack = builder.zeroBack;
        this.selfDrivingLevel = builder.selfDrivingLevel;
        this.frontTireSpecification = builder.frontTireSpecification;
        this.rearTireSpecification = builder.rearTireSpecification;
        this.personnelCount = builder.personnelCount;

    }

    public static class TrimBuilder implements CommonModelBuilder<Trim> {
        private final Grade grade;
        private final String trimName;
        private final Double price;
        private final FuelType fuelType;
        private final String engineType;
        private final Integer displacement;
        private final String driveMethod;
        private final Integer highOutput;
        private final Double maximumTorque;
        private final Double combinedFuel;
        private final Double cityFuel;
        private final Double highwayFuel;
        private final Integer fuelRating;
        private final Double co2Emissions;
        private final Integer lowPollutionGrade;
        private final Double toleranceWeight;
        private final Integer maximumSpeed;
        private final Double zeroBack;
        private final Integer selfDrivingLevel;
        private final String frontTireSpecification;
        private final String rearTireSpecification;
        private final Integer personnelCount;

        public TrimBuilder(Grade grade, TrimRequest request) {
            this.grade = grade;
            this.trimName = request.getTrimName();
            this.price = request.getPrice();
            this.fuelType = request.getFuelType();
            this.engineType = request.getEngineType();
            this.displacement = request.getDisplacement();
            this.driveMethod = request.getDriveMethod();
            this.highOutput = request.getHighOutput();
            this.maximumTorque = request.getMaximumTorque();
            this.combinedFuel = request.getCombinedFuel();
            this.cityFuel = request.getCityFuel();
            this.highwayFuel = request.getHighwayFuel();
            this.fuelRating = request.getFuelRating();
            this.co2Emissions = request.getCo2Emissions();
            this.lowPollutionGrade = request.getLowPollutionGrade();
            this.toleranceWeight = request.getToleranceWeight();
            this.maximumSpeed = request.getMaximumSpeed();
            this.zeroBack = request.getZeroBack();
            this.selfDrivingLevel = request.getSelfDrivingLevel();
            this.frontTireSpecification = request.getFrontTireSpecification();
            this.rearTireSpecification = request.getRearTireSpecification();
            this.personnelCount = request.getPersonnelCount();

        }

        @Override
        public Trim build() {
            return new Trim(this);
        }
    }


}
