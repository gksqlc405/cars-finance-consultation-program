package com.chb.carConsultation.entity;

import com.chb.carConsultation.interfaces.CommonModelBuilder;
import com.chb.carConsultation.model.carManufacturer.ManufacturerRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Manufacturer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String carManufacturer;

    public void putManufacturer(ManufacturerRequest request) {
        this.carManufacturer = request.getCarManufacturer();
    }

    private Manufacturer(ManufacturerBuilder builder) {
        this.carManufacturer = builder.carManufacturer;

    }

    public static class ManufacturerBuilder implements CommonModelBuilder<Manufacturer> {
        private final String carManufacturer;

        public ManufacturerBuilder(ManufacturerRequest request) {
            this.carManufacturer = request.getCarManufacturer();
        }

        @Override
        public Manufacturer build() {
            return new Manufacturer(this);
        }
    }


}
