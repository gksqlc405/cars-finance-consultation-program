package com.chb.carConsultation.entity;

import com.chb.carConsultation.interfaces.CommonModelBuilder;
import com.chb.carConsultation.model.UsageDetail.UsageDetailPhoneUpdate;
import com.chb.carConsultation.model.UsageDetail.UsageDetailRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "trimId", nullable = false)
    private Trim trim;

    @Column(nullable = false,length = 15)
    private String member;

    @Column(nullable = false, length = 20)
    private String phone;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    public void putPhoneUpdate(UsageDetailPhoneUpdate phoneUpdate) {
        this.phone = phoneUpdate.getPhone();
    }

    public void putUsageDetail(UsageDetailRequest request) {
        this.member = request.getMember();
        this.phone = request.getPhone();
    }

    private UsageDetail(UsageDetailBuilder builder) {
        this.trim = builder.trim;
        this.member = builder.member;
        this.phone = builder.phone;
        this.dateCreate = builder.dateCreate;

    }

    public static class UsageDetailBuilder implements CommonModelBuilder<UsageDetail> {

        private final Trim trim;
        private final String member;
        private final String phone;
        private final LocalDateTime dateCreate;

        public UsageDetailBuilder(Trim trim, UsageDetailRequest request) {
            this.trim = trim;
            this.member = request.getMember();
            this.phone = request.getPhone();
            this.dateCreate = LocalDateTime.now();
        }

        @Override
        public UsageDetail build() {
            return new UsageDetail(this);
        }
    }
}
