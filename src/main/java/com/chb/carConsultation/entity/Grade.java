package com.chb.carConsultation.entity;

import com.chb.carConsultation.interfaces.CommonModelBuilder;
import com.chb.carConsultation.model.carGrade.GradeRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Grade {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carModelId", nullable = false)
    private CarModel carModel;

    @Column(nullable = false, length = 20)
    private String carGrade;

    public void putGrade(GradeRequest request) {
        this.carGrade = request.getCarGrade();
    }

    private Grade(GradeBuilder builder) {
        this.carModel = builder.carModel;
        this.carGrade = builder.carGrade;

    }

    public static class GradeBuilder implements CommonModelBuilder<Grade> {
        private final CarModel carModel;
        private final String carGrade;

        public GradeBuilder(CarModel carModel, GradeRequest request) {
            this.carModel = carModel;
            this.carGrade = request.getCarGrade();
        }

        @Override
        public Grade build() {
            return new Grade(this);
        }
    }




}
