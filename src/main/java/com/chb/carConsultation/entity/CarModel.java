package com.chb.carConsultation.entity;

import com.chb.carConsultation.interfaces.CommonModelBuilder;
import com.chb.carConsultation.model.carModel.CarModelLaunchUpdateRequest;
import com.chb.carConsultation.model.carModel.CarModelRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarModel {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "manufacturerId", nullable = false)
    private Manufacturer manufacturer;

    @Column(nullable = false, length = 30)
    private String modelName;

    @Column(nullable = false, length = 20)
    private String external;

    @Column(nullable = false, length = 20)
    private String type;

    @Column(nullable = false, length = 10)
    private String launch;

    @Column(nullable = false, length = 15)
    private String gearbox;

    @Column(nullable = false)
    private Integer yearModel;

    public void putCarModel(CarModelRequest request) {
        this.modelName = request.getModelName();
        this.external = request.getExternal();
        this.type = request.getType();
        this.launch = request.getLaunch();
        this.gearbox = request.getGearbox();
        this.yearModel = request.getYearModel();
    }

    public void putLaunchUpdate(CarModelLaunchUpdateRequest request) {
        this.launch = request.getLaunch();

    }

    private CarModel(CarModelBuilder builder) {
        this.manufacturer = builder.manufacturer;
        this.modelName = builder.modelName;
        this.external = builder.external;
        this.type = builder.type;
        this.launch = builder.launch;
        this.gearbox = builder.gearbox;
        this.yearModel = builder.yearModel;
    }

    public static class CarModelBuilder implements CommonModelBuilder<CarModel> {
        private final Manufacturer manufacturer;
        private final String modelName;
        private final String external;
        private final String type;
        private final String launch;
        private final String gearbox;
        private final Integer yearModel;

        public CarModelBuilder(Manufacturer manufacturer ,CarModelRequest request) {
            this.manufacturer = manufacturer;
            this.modelName = request.getModelName();
            this.external = request.getExternal();
            this.type = request.getType();
            this.launch = request.getLaunch();
            this.gearbox = request.getGearbox();
            this.yearModel = request.getYearModel();


        }

        @Override
        public CarModel build() {
            return new CarModel(this);
        }
    }



}
