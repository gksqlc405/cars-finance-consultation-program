package com.chb.carConsultation.repository;

import com.chb.carConsultation.entity.Grade;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GradeRepository extends JpaRepository<Grade, Long> {
}
