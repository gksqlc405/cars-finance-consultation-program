package com.chb.carConsultation.repository;

import com.chb.carConsultation.entity.Trim;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TrimRepository extends JpaRepository<Trim, Long> {
    List<Trim> findAllByGrade_CarModel_Id(Long carModelId);
}
