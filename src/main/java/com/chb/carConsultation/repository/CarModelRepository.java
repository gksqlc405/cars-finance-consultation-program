package com.chb.carConsultation.repository;


import com.chb.carConsultation.entity.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarModelRepository extends JpaRepository<CarModel, Long> {
}
