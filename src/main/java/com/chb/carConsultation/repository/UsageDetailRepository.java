package com.chb.carConsultation.repository;

import com.chb.carConsultation.entity.UsageDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface UsageDetailRepository extends JpaRepository<UsageDetail, Long> {
    List<UsageDetail> findAllByDateCreateGreaterThanEqualAndDateCreateLessThanEqualOrderByIdDesc(LocalDateTime dateStart, LocalDateTime dateEnd);
}
