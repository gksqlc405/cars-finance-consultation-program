package com.chb.carConsultation.model.carModel;

import com.chb.carConsultation.entity.CarModel;
import com.chb.carConsultation.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarModelItem {
    @NotNull
    @ApiModelProperty(notes = "차량모델 시퀀스")
    private Long carModelId;
    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "차량 제조사 + 모델명")
    private String carFullName;
    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "차량 외관 + 차종")
    private String allType;
    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "출시")
    private String launch;
    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "변속기")
    private String gearbox;
    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "연식")
    private String yearModel;

    @NotNull
    @ApiModelProperty(notes = "제조사 시퀀스")
    private Long manufacturerId;


    private CarModelItem(CarModelItemBuilder builder) {
        this.carModelId = builder.carModelId;
        this.carFullName = builder.carFullName;
        this.allType = builder.allType;
        this.launch = builder.launch;
        this.gearbox = builder.gearbox;
        this.yearModel = builder.yearModel;
        this.manufacturerId = builder.manufacturerId;

    }

    public static class CarModelItemBuilder implements CommonModelBuilder<CarModelItem> {
        private final Long carModelId;
        private final String carFullName;
        private final String allType;
        private final String launch;
        private final String gearbox;
        private final String yearModel;
        private final Long manufacturerId;


        public CarModelItemBuilder(CarModel carModel) {
            this.carModelId = carModel.getId();
            this.carFullName = carModel.getManufacturer().getCarManufacturer() + " " + carModel.getModelName();
            this.allType = carModel.getType() + " " + carModel.getExternal();
            this.launch = carModel.getLaunch();
            this.gearbox = carModel.getGearbox();
            this.yearModel = carModel.getYearModel() + "년형";
            this.manufacturerId = carModel.getManufacturer().getId();

        }

        @Override
        public CarModelItem build() {
            return new CarModelItem(this);
        }
    }
}
