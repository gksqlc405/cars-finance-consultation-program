package com.chb.carConsultation.model.carModel;

import com.chb.carConsultation.entity.Manufacturer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarModelRequest {


    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "차량 모델명", required = true)
    private String modelName;

    @NotNull
    @Length(min = 1, max = 10)
    @ApiModelProperty(notes = "외관", required = true)
    private String external;

    @NotNull
    @Length(min = 1, max = 15)
    @ApiModelProperty(notes = "차량 타입", required = true)
    private String type;

    @NotNull
    @Length(min = 1, max = 10)
    @ApiModelProperty(notes = "출시", required = true)
    private String launch;

    @NotNull
    @Length(min = 1, max = 10)
    @ApiModelProperty(notes = "기어변속기", required = true)
    private String gearbox;

    @NotNull
    @ApiModelProperty(notes = "연식", required = true)
    private Integer yearModel;
}
