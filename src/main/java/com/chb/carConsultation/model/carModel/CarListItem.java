package com.chb.carConsultation.model.carModel;

import com.chb.carConsultation.entity.CarModel;
import com.chb.carConsultation.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarListItem {
    private String launchName;

    private String carFullName;

    private CarListItem(CarListItemBuilder builder) {
        this.launchName = builder.launchName;
        this.carFullName = builder.carFullName;

    }

    public static class CarListItemBuilder implements CommonModelBuilder<CarListItem> {
        private final String launchName;
        private final String carFullName;

        public CarListItemBuilder(CarModel carModel) {
            this.launchName = carModel.getLaunch();
            this.carFullName = carModel.getManufacturer().getCarManufacturer() + " " + carModel.getModelName() +
                    carModel.getExternal() + "(" + carModel.getType() + ")";
        }


        @Override
        public CarListItem build() {
            return new CarListItem(this);
        }
    }
}
