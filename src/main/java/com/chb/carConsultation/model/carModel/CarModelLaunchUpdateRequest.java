package com.chb.carConsultation.model.carModel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarModelLaunchUpdateRequest {

    @NotNull
    @Length(min = 1, max = 5)
    @ApiModelProperty(notes = "출시", required = true)
    private String launch;
}
