package com.chb.carConsultation.model.carGrade;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class GradeRequest {
    @NotNull
    @Length(min = 2, max = 15)
    @ApiModelProperty(notes = "차량 등급")
    private String carGrade;
}
