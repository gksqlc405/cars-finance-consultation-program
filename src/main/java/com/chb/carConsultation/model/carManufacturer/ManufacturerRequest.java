package com.chb.carConsultation.model.carManufacturer;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ManufacturerRequest {
    @NotNull
    @Length(min = 1,max = 20)
    @ApiModelProperty(notes = "제조사명", required = true)
    private String carManufacturer;
}
