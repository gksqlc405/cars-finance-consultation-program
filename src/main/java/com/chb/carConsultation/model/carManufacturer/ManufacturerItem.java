package com.chb.carConsultation.model.carManufacturer;

import com.chb.carConsultation.entity.Manufacturer;
import com.chb.carConsultation.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ManufacturerItem {

    @NotNull
    @ApiModelProperty(notes = "제조시 시퀀스")
    private Long id;

    @NotNull
    @Length(min = 1, max = 15)
    @ApiModelProperty(notes = "제조사명")
    private String carManufacturer;

    private ManufacturerItem(ManufacturerItemBuilder builder) {
        this.id = builder.id;
        this.carManufacturer = builder.carManufacturer;
    }

    public static class ManufacturerItemBuilder implements CommonModelBuilder<ManufacturerItem> {
        private final Long id;
        private final String carManufacturer;

        public ManufacturerItemBuilder(Manufacturer manufacturer) {
            this.id = manufacturer.getId();
            this.carManufacturer = manufacturer.getCarManufacturer();
        }

        @Override
        public ManufacturerItem build() {
            return new ManufacturerItem(this);
        }
    }

}
