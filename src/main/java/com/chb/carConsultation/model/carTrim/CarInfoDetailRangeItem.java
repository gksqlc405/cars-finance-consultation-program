package com.chb.carConsultation.model.carTrim;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CarInfoDetailRangeItem {
    private Double minDoubleValue;

    private Double maxDoubleValue;

    private Integer minIntValue;

    private Integer maxIntValue;
}
