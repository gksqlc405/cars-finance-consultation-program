package com.chb.carConsultation.model.carTrim;

import com.chb.carConsultation.enums.FuelType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class TrimRequest {
    @NotNull
    @Length(min = 2, max = 15)
    @ApiModelProperty(notes = "트림명", required = true)
    private String trimName;

    @NotNull
    @ApiModelProperty(notes = "차량 가격", required = true)
    private Double price;

    @NotNull
    @ApiModelProperty(notes = "연료", required = true)
    private FuelType fuelType;

    @NotNull
    @Length(min = 2, max = 15)
    @ApiModelProperty(notes = "엔진형식", required = true)
    private String engineType;

    @NotNull
    @ApiModelProperty(notes = "배기량", required = true)
    private Integer displacement;

    @NotNull
    @Length(min = 2, max = 10)
    @ApiModelProperty(notes = "구동방식", required = true)
    private String driveMethod;

    @NotNull
    @ApiModelProperty(notes = "최고출력", required = true)
    private Integer highOutput;

    @NotNull
    @ApiModelProperty(notes = "최대토크", required = true)
    private Double maximumTorque;

    @NotNull
    @ApiModelProperty(notes = "복합연비", required = true)
    private Double combinedFuel;

    @NotNull
    @ApiModelProperty(notes = "도심연비", required = true)
    private Double cityFuel;

    @NotNull
    @ApiModelProperty(notes = "고속도로연비", required = true)
    private Double highwayFuel;

    @NotNull
    @ApiModelProperty(notes = "연비등급", required = true)
    private Integer fuelRating;

    @NotNull
    @ApiModelProperty(notes = "Co2배출량", required = true)
    private Double co2Emissions;

    @NotNull
    @ApiModelProperty(notes = "저공해등급", required = true)
    private Integer lowPollutionGrade;

    @NotNull
    @ApiModelProperty(notes = "공차중량", required = true)
    private Double toleranceWeight;

    @NotNull
    @ApiModelProperty(notes = "최고속도", required = true)
    private Integer maximumSpeed;

    @NotNull
    @ApiModelProperty(notes = "제로백", required = true)
    private Double zeroBack;

    @NotNull
    @ApiModelProperty(notes = "자율주행 레벨", required = true)
    private Integer selfDrivingLevel;

    @NotNull
    @Length(min = 3,max = 15)
    @ApiModelProperty(notes = "앞바퀴 사양", required = true)
    private String frontTireSpecification;

    @NotNull
    @Length(min = 3,max = 15)
    @ApiModelProperty(notes = "뒷바퀴 사양", required = true)
    private String rearTireSpecification;

    @NotNull
    @ApiModelProperty(notes = "정원")
    private Integer personnelCount;
}
