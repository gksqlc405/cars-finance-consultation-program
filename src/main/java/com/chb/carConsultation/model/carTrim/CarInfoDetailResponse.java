package com.chb.carConsultation.model.carTrim;

import com.chb.carConsultation.entity.CarModel;
import com.chb.carConsultation.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarInfoDetailResponse {
    private String makerLogoUrl;

    private String launch;

    private CarInfoDetailRangeItem priceItem;

    private String carType;

    private List<String> carFuelTypes;

    private CarInfoDetailRangeItem displacementItem;

    private CarInfoDetailRangeItem fuelEfficiencyItem;

    private CarInfoDetailRangeItem personnelCountItem;

    private CarInfoDetailResponse(CarInfoDetailResponseBuilder builder) {
        this.makerLogoUrl = builder.makerLogoUrl;
        this.launch = builder.launch;
        this.priceItem = builder.priceItem;
        this.carType = builder.carType;
        this.carFuelTypes = builder.carFuelTypes;
        this.displacementItem = builder.displacementItem;
        this.fuelEfficiencyItem = builder.fuelEfficiencyItem;
        this.personnelCountItem = builder.personnelCountItem;

    }

    public static class CarInfoDetailResponseBuilder implements CommonModelBuilder<CarInfoDetailResponse> {
        private final String makerLogoUrl;

        private final String launch;

        private final CarInfoDetailRangeItem priceItem;

        private final String carType;

        private final List<String> carFuelTypes;

        private final CarInfoDetailRangeItem displacementItem;

        private final CarInfoDetailRangeItem fuelEfficiencyItem;

        private final CarInfoDetailRangeItem personnelCountItem;

        public CarInfoDetailResponseBuilder(
                CarModel carModel,
                Double minPrice,
                Double maxPrice,
                List<String> carFuelTypes,
                Integer minDisplacement,
                Integer maxDisplacement,
                Double minFuelEfficiency,
                Double maxFuelEfficiency,
                Integer minPersonnelCount,
                Integer maxPersonnelCount

        ) {
            this.makerLogoUrl = carModel.getManufacturer().getCarManufacturer();
            this.launch = carModel.getLaunch();
            this.carType = carModel.getExternal() + "(" + carModel.getType() + ")";

            CarInfoDetailRangeItem priceItem = new CarInfoDetailRangeItem();
            priceItem.setMinDoubleValue(minPrice);
            priceItem.setMaxDoubleValue(maxPrice);
            this.priceItem = priceItem;

            this.carFuelTypes = carFuelTypes;

            CarInfoDetailRangeItem displacementItem = new CarInfoDetailRangeItem();
            displacementItem.setMinIntValue(minDisplacement);
            displacementItem.setMaxIntValue(maxDisplacement);
            this.displacementItem = displacementItem;

            CarInfoDetailRangeItem fuelEfficiencyItem = new CarInfoDetailRangeItem();
            fuelEfficiencyItem.setMinDoubleValue(minFuelEfficiency);
            fuelEfficiencyItem.setMaxDoubleValue(maxFuelEfficiency);
            this.fuelEfficiencyItem = fuelEfficiencyItem;

            CarInfoDetailRangeItem personnelCountItem = new CarInfoDetailRangeItem();
            personnelCountItem.setMinIntValue(minPersonnelCount);
            personnelCountItem.setMaxIntValue(maxPersonnelCount);
            this.personnelCountItem = personnelCountItem;

        }

        @Override
        public CarInfoDetailResponse build() {
            return new CarInfoDetailResponse(this);
        }
    }
}
