package com.chb.carConsultation.model.carTrim;

import com.chb.carConsultation.enums.FuelType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class FuelTypeUpdateRequest {

    @Enumerated(value = EnumType.STRING)
    @NotNull
    @ApiModelProperty(notes = "연료", required = true)
    private FuelType fuelType;
}
