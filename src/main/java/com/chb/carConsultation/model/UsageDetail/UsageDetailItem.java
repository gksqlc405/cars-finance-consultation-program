package com.chb.carConsultation.model.UsageDetail;

import com.chb.carConsultation.entity.UsageDetail;
import com.chb.carConsultation.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageDetailItem {
    @NotNull
    @Length(min = 1,max = 10)
    @ApiModelProperty(notes = "회원이름")
    private String member;

    @NotNull
    @Length(min = 13, max = 13)
    @ApiModelProperty(notes = "회원 전화번호")
    private String phone;

    @NotNull
    @ApiModelProperty(notes = "상담내역 날짜")
    private LocalDateTime dateCreate;

    private UsageDetailItem(UsageDetailItemBuilder builder) {
        this.member = builder.member;
        this.phone = builder.phone;
        this.dateCreate = builder.dateCreate;


    }

    public static class UsageDetailItemBuilder implements CommonModelBuilder<UsageDetailItem>{
        private final String member;
        private final String phone;

        private final LocalDateTime dateCreate;

        public UsageDetailItemBuilder(UsageDetail usageDetail) {
            this.member = usageDetail.getMember();
            this.phone = usageDetail.getPhone();
            this.dateCreate = usageDetail.getDateCreate();

        }


        @Override
        public UsageDetailItem build() {
            return new UsageDetailItem(this);
        }
    }
}
