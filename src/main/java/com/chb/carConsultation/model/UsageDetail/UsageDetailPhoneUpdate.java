package com.chb.carConsultation.model.UsageDetail;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class UsageDetailPhoneUpdate {
    @NotNull
    @Length(min = 13, max = 13)
    @ApiModelProperty(notes = "회원 전화번호", required = true)
    private String phone;
}
