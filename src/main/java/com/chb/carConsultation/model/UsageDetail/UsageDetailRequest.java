package com.chb.carConsultation.model.UsageDetail;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class UsageDetailRequest {

    @NotNull
    @Length(min = 1, max = 10)
    @ApiModelProperty(notes = "회원이름", required = true)
    private String member;

    @NotNull
    @Length(min = 13, max = 13)
    @ApiModelProperty(notes = "회원 전화번호", required = true)
    private String phone;
}
