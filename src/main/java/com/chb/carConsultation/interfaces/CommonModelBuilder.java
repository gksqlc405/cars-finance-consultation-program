package com.chb.carConsultation.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
