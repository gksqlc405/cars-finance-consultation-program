package com.chb.carConsultation.controller;

import com.chb.carConsultation.entity.Trim;
import com.chb.carConsultation.model.*;
import com.chb.carConsultation.model.UsageDetail.UsageDetailItem;
import com.chb.carConsultation.model.UsageDetail.UsageDetailPhoneUpdate;
import com.chb.carConsultation.model.UsageDetail.UsageDetailRequest;
import com.chb.carConsultation.service.CarInfoService;
import com.chb.carConsultation.service.ResponseService;
import com.chb.carConsultation.service.UsageDetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "상담내역 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/usage-detail")
public class UsageDetailController {
    private final UsageDetailService usageDetailService;
    private final CarInfoService carInfoService;

    @ApiOperation(value = "상담내역 등록")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "setTrimId", value = "트림 시퀀스", required = true)
    })
    @PostMapping("/set-trim/{setTrimId}")
    public CommonResult setUsageDetail(@PathVariable long setTrimId, @RequestBody @Valid UsageDetailRequest request) {
        Trim trim = carInfoService.getTrimId(setTrimId);
        usageDetailService.setUsageDetail(trim, request);
        return ResponseService.getSuccessResult();

    }


    @ApiOperation(value = "상담내역 정보 가저오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "상담내역 시퀀스", required = true)
    })
    @GetMapping("/{id}")
    public SingleResult<UsageDetailItem> getUsageDetail(@PathVariable long id) {
        return ResponseService.getSingleResult(usageDetailService.getUsageDetail(id));
    }

    @ApiOperation(value = "상담내역 정보 모두 가저오기")
    @GetMapping("/all")
    public ListResult<UsageDetailItem> getUsageDetails() {
        return ResponseService.getListResult(usageDetailService.getUsageDetails(), true);
    }


    @ApiOperation(value = "상담내역 정보 수정")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "상담내역 시퀀스", required = true)
    })
    @PutMapping("/id/{id}")
    public CommonResult putUsageDetail(@PathVariable long id, @RequestBody @Valid UsageDetailRequest request) {
        usageDetailService.putUsageDetail(id, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 전화번호 수정")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "phoneId", value = "회원전화번호 수정 시퀀스", required = true)
    })
    @PutMapping("/phone/{phoneId}")
    public CommonResult putPhoneUpdate(@PathVariable long phoneId, @RequestBody @Valid UsageDetailPhoneUpdate request) {
        usageDetailService.putPhoneUpdate(phoneId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "상담내역 날짜 확인")
    @GetMapping("/search")
    public ListResult<UsageDetailItem> getUsageDetail(
            @RequestParam(value = "dateStart") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateStart,
            @RequestParam(value = "dateEnd")  @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateEnd
    ) {
        return ResponseService.getListResult(usageDetailService.getUsageDetail(dateStart, dateEnd), true);
    }
}



