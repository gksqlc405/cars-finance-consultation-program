package com.chb.carConsultation.controller;


import com.chb.carConsultation.entity.CarModel;
import com.chb.carConsultation.entity.Grade;
import com.chb.carConsultation.entity.Manufacturer;
import com.chb.carConsultation.model.*;
import com.chb.carConsultation.model.carGrade.GradeRequest;
import com.chb.carConsultation.model.carManufacturer.ManufacturerItem;
import com.chb.carConsultation.model.carManufacturer.ManufacturerRequest;
import com.chb.carConsultation.model.carModel.CarModelItem;
import com.chb.carConsultation.model.carModel.CarModelLaunchUpdateRequest;
import com.chb.carConsultation.model.carModel.CarModelRequest;
import com.chb.carConsultation.model.carTrim.CarInfoDetailResponse;
import com.chb.carConsultation.model.carTrim.FuelTypeUpdateRequest;
import com.chb.carConsultation.model.carTrim.TrimRequest;
import com.chb.carConsultation.service.CarInfoService;
import com.chb.carConsultation.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "차량 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/car-info")
public class CarInfoController {
    private final CarInfoService carInfoService;

    @ApiOperation(value = "제조사 정보 등록")
    @PostMapping("/new")
    public CommonResult setManufacturer(@RequestBody @Valid ManufacturerRequest request) {
        carInfoService.setManufacturer(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "제조사 정보 하나 가저오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "singleId", value = "제조사 시퀀스", required = true)
    })
    @GetMapping("/single/{singleId}")
    public SingleResult<ManufacturerItem> getManufacturer(@PathVariable long singleId) {
        return ResponseService.getSingleResult(carInfoService.getManufacturer(singleId));
    }

    @ApiOperation(value = "제조사 정보 모두가저오기")
    @GetMapping("/all")
    public ListResult<ManufacturerItem> getManufacturers() {
        return ResponseService.getListResult(carInfoService.getManufacturers(), true);
    }

    @ApiOperation(value = "제조사명 수정하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "putId", value = "제조사명 시퀀스", required = true),

    })
    @PutMapping("/put/{putId}")
    public CommonResult putManufacturer(@PathVariable long putId, @RequestBody @Valid  ManufacturerRequest request) {
        carInfoService.putManufacturer(putId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "차량모델 정보 등록")
    @PostMapping("/manufacturer/{manufacturerId}")
    public CommonResult setCarModel(@PathVariable long manufacturerId, @RequestBody @Valid CarModelRequest request) {
        Manufacturer manufacturer = carInfoService.getManufacturerId(manufacturerId);
        carInfoService.setCarModel(manufacturer, request);
        return ResponseService.getSuccessResult();

    }

    @ApiOperation(value = "차량모델 정보 가저오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "getId", value = "차량모델 시퀀스", required = true)
    })
    @GetMapping("/get-model/{getId}")
    public SingleResult<CarModelItem> getCarModel(@PathVariable long getId) {
        return ResponseService.getSingleResult(carInfoService.getCarModels(getId));
    }

    @ApiOperation(value = "차량모델 정보 모두 가저오기")
    @GetMapping("/modelAll")
    public ListResult<CarModelItem> getCarModels() {
        return ResponseService.getListResult(carInfoService.getCarModels(), true);
    }

    @ApiOperation(value = "출시일 수정하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "launchId", value = "차량모델 출시일 시퀀스", required = true)
    })
    @PutMapping("/launch/{launchId}")
    public CommonResult putLaunchUpdate(@PathVariable long launchId, @RequestBody  @Valid CarModelLaunchUpdateRequest request) {
        carInfoService.putLaunchUpdate(launchId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "차량모델 정보 수정하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "carModelId", value = "차량모델 시퀀스", required = true)
    })
    @PutMapping("/car-model-id/{carModelId}")
    public CommonResult putCarModel(@PathVariable long carModelId, @RequestBody @Valid CarModelRequest request) {
        carInfoService.putCarModel(carModelId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "차량 등급 등록")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "gradeModelId", value = "차량모델 시퀀스", required = true)
    })
    @PostMapping("/grade-model/{gradeModelId}")
    public CommonResult setGrade(@PathVariable long gradeModelId, @RequestBody @Valid GradeRequest request) {
        CarModel carModel = carInfoService.getCarModelId(gradeModelId);
        carInfoService.setGrade(carModel, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "차량등급 정보 수정")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "putGradeId", value = "차량등급 시퀀스", required = true)
    })
    @PutMapping("/put-grade/{putGradeId}")
    public CommonResult putGrade(@PathVariable long putGradeId, @RequestBody @Valid GradeRequest request) {
        carInfoService.putGrade(putGradeId,request);
        return ResponseService.getSuccessResult();

    }

    @ApiOperation(value = "트림 정보 등록")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "gradeId", value = "차량등급 시퀀스", required = true)
    })
    @PostMapping("/grade/{gradeId}")
    public CommonResult setTrim(@PathVariable long gradeId, @RequestBody @Valid TrimRequest request) {
        Grade grade = carInfoService.getGradeId(gradeId);
        carInfoService.setTrim(grade,request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "트림 정보 수정")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "trimId", value = "트림 시퀀스", required = true)
    })
    @PutMapping("/trimId/{trimId}")
    public CommonResult putTrim(@PathVariable long trimId, @RequestBody @Valid TrimRequest request) {
        carInfoService.putTrim(trimId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "연료타입 수정")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fuelTypeId", value = "연료타입 수정 시퀀스", required = true)
    })
    @PutMapping("/fuel-type/{fuelTypeId}")
    public CommonResult putFuelType(@PathVariable long fuelTypeId, @RequestBody @Valid FuelTypeUpdateRequest request) {
        carInfoService.putFuelTpeUpdate(fuelTypeId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "자동차 디테일 정보 가저오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "infoDetailId", value = "차량 디테일 시퀀스", required = true)
    })
    @GetMapping("/info-detail/{infoDetailId}")
    public SingleResult<CarInfoDetailResponse> getCarInfoDetail(@PathVariable long infoDetailId) {
        return ResponseService.getSingleResult(carInfoService.getCarInfoDetail(infoDetailId));
    }



}
